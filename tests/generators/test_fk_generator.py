import os
import sys
from datetime import timedelta

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))

from pgsynthdata.database import simple_column_pg_infos
from pgsynthdata.datagenerator import empty_contraints, empty_statistics
from pgsynthdata.generators.fk_generator import fk_generator


def test_fk_int():

    # fixture
    input_fks = [50, 60, 70, 80, 90, 100, 110, 120]
    info = simple_column_pg_infos("integer")
    const = empty_contraints()
    const.is_fk = True
    const.fk_values = input_fks
    stats = empty_statistics()
    stats = empty_statistics()
    stats.custom_stats.update(
        {
            "absolut_min": [[min(input_fks)]],
            "absolut_max": [[max(input_fks)]],
        }
    )

    # run
    generated = fk_generator(stats, info, const, len(input_fks))

    # assert
    assert len(generated) == len(
        input_fks
    ), f"len of generated is not same as input fk's"
    for v in generated:
        assert (
            v in input_fks
        ), f"generated value '{v}' is not in input fk's '{input_fks}'"


def test_fk_date():

    # fixture
    from datetime import date

    input_fks = [date.today() + timedelta(days=i) for i in range(10)]
    info = simple_column_pg_infos("integer", "int8")
    const = empty_contraints()
    const.is_fk = True
    const.fk_values = input_fks
    stats = empty_statistics()
    stats = empty_statistics()
    stats.custom_stats.update(
        {
            "absolut_min": [[min(input_fks)]],
            "absolut_max": [[max(input_fks)]],
        }
    )

    # run
    generated = fk_generator(stats, info, const, len(input_fks))

    # assert
    assert len(generated) == len(
        input_fks
    ), f"len of generated is not same as input fk's"
    for v in generated:
        assert (
            v in input_fks
        ), f"generated value '{v}' is not in input fk's '{input_fks}'"


def test_fk_text():

    # fixture
    input_fks = ["a", "b", "c", "dd", "eee", "ffff"]
    info = simple_column_pg_infos("text", "")
    const = empty_contraints()
    const.is_fk = True
    const.fk_values = input_fks
    stats = empty_statistics()
    stats = empty_statistics()
    stats.custom_stats.update(
        {
            "absolut_min": [[min(input_fks)]],
            "absolut_max": [[max(input_fks)]],
        }
    )

    # run
    generated = fk_generator(stats, info, const, len(input_fks))

    # assert
    assert len(generated) == len(
        input_fks
    ), f"len of generated is not same as input fk's"
    for v in generated:
        assert (
            v in input_fks
        ), f"generated value '{v}' is not in input fk's '{input_fks}'"
