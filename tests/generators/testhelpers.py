from typing import Optional, Set

from pgsynthdata.database import empty_column_pg_infos, simple_column_pg_infos
from pgsynthdata.datagenerator import (
    Generator,
    empty_contraints,
    empty_statistics,
    get_generator,
)
from pgsynthdata.main import DbStructure


def registration(
    DATA_TYPES: Set[str], generator: Generator, udt_name: Optional[str] = None
):
    for data_type in DATA_TYPES:
        col_info = simple_column_pg_infos(data_type, udt_name)
        constraints = empty_contraints()
        actual = get_generator(col_info, constraints)
        assert (
            actual.function == generator
        ), f"false generator for data_type {data_type}"


def testdb(
    TESTDB: DbStructure,
    DATA_TYPES: Set[str],
    generator: Generator,
    expected_types,
    skip_unique=False,
):
    testtable = TESTDB[generator.__name__]
    not_yet_tested_types = DATA_TYPES.copy()
    for colname, col in testtable.columns.items():
        if col.pg_info.data_type in DATA_TYPES and not (
            col.constraints.is_unique and skip_unique
        ):
            not_yet_tested_types.discard(col.pg_info.data_type)
            generated = generator(
                col.stats, col.pg_info, col.constraints, testtable.rows_to_generate
            )
            assert (
                generated != None
            ), f"no data generated ({testtable.pg_info.name}.{colname})"
            assert (
                len(generated) == testtable.rows_to_generate
            ), f"generated length is {len(generated)} / expected {testtable.rows_to_generate} ({testtable.pg_info.name}.{colname})"
            not_none_sample = [x for x in generated if x is not None][0]
            assert isinstance(
                not_none_sample, expected_types
            ), f"generated type is {type(generated[0])} / expected {expected_types} ({testtable.pg_info.name}.{colname})"
    assert (
        len(not_yet_tested_types) == 0
    ), f"datatype(s) {str(not_yet_tested_types)} where not tested"


def fallbacks(
    DATA_TYPES: Set[str],
    generator: Generator,
    expected_types,
    stats=empty_statistics(),
    column_info=empty_column_pg_infos(),
    constraints=empty_contraints(),
):
    ROWS_TO_GEN = 10
    for data_type in DATA_TYPES:
        column_info.data_type = data_type
        generated = generator(stats, column_info, constraints, ROWS_TO_GEN)
        assert len(generated) == ROWS_TO_GEN
        assert isinstance(generated[0], expected_types)
