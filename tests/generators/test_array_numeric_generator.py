from typing import Dict

from pgsynthdata.generators.array_numeric_generator import array_numeric_generator
from pgsynthdata.main import Table

from .. import testdb_pickler
from . import testhelpers as helpers

TESTDB: Dict[str, Table] = testdb_pickler.unpickle_db()
DATA_TYPES = {"ARRAY"}
GENERATOR = array_numeric_generator
EXPECTED_TYPES = list


def test_registration():
    helpers.registration(DATA_TYPES, GENERATOR, udt_name="_int4")


def test_generate_testdb():
    helpers.testdb(TESTDB, DATA_TYPES, GENERATOR, EXPECTED_TYPES)
