from pgsynthdata.generators.boolean_generator import boolean_generator

from .. import testdb_pickler
from . import testhelpers as helpers

TESTDB_TABLES = testdb_pickler.unpickle_db()
DATA_TYPES = {"boolean"}
GENERATOR = boolean_generator
EXPECTED_TYPES = bool


def test_registration():
    helpers.registration(DATA_TYPES, GENERATOR)


def test_generate_testdb():

    helpers.testdb(
        TESTDB_TABLES, DATA_TYPES, GENERATOR, EXPECTED_TYPES, skip_unique=True
    )


def test_fallbacks():
    helpers.fallbacks(DATA_TYPES, GENERATOR, EXPECTED_TYPES)


# def test_generator():
#     stats = empty_Statistics()
#     col_info = empty_ColumnPgInfos()
#     constraints = empty_Contraints()

#     ROWS_TO_GEN = 10

#     generated = boolean_generator(stats, col_info, constraints, ROWS_TO_GEN)

#     assert len(generated) == ROWS_TO_GEN
#     assert generated[0] in {"true", "false"}  # return string 'true'/'false'
