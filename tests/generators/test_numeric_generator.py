from pgsynthdata.generators.numeric_generator import numeric_generator

from .. import testdb_pickler
from . import testhelpers as helpers

TESTDB_TABLES = testdb_pickler.unpickle_db()

DATA_TYPES = {
    "integer",
    "smallint",
    "bigint",
    "numeric",
    "double precision",
    "real",
}
GENERATOR = numeric_generator
EXPECTED_TYPES = (int, float)


def test_registration():
    helpers.registration(DATA_TYPES, GENERATOR)


def test_generate_testdb():
    helpers.testdb(TESTDB_TABLES, DATA_TYPES, GENERATOR, EXPECTED_TYPES)


def test_fallbacks():
    stats = helpers.empty_statistics()
    stats.custom_stats.update(
        {
            "absolut_min": [[None]],
            "absolut_max": [[None]],
        }
    )
    helpers.fallbacks(DATA_TYPES, GENERATOR, EXPECTED_TYPES, stats=stats)
