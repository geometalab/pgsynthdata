from pgsynthdata.datagenerator import AsIs
from pgsynthdata.generators.gan_generator import gan_generator

from .. import testdb_pickler
from . import testhelpers as helpers

TESTDB_TABLES = testdb_pickler.unpickle_db()
DATA_TYPES = {"USER-DEFINED", "double precision"}
GENERATOR = gan_generator
EXPECTED_TYPES = (AsIs, float)


def test_generate_testdb():
    helpers.testdb(TESTDB_TABLES, DATA_TYPES, GENERATOR, EXPECTED_TYPES)
