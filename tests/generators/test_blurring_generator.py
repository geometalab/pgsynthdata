from pgsynthdata.datagenerator import AsIs
from pgsynthdata.generators.blurring_generator import blurring_generator

from .. import testdb_pickler
from . import testhelpers as helpers

TESTDB_TABLES = testdb_pickler.unpickle_db()
DATA_TYPES = {"USER-DEFINED"}
GENERATOR = blurring_generator
EXPECTED_TYPES = AsIs


def test_generate_testdb():
    helpers.testdb(TESTDB_TABLES, DATA_TYPES, GENERATOR, EXPECTED_TYPES)
