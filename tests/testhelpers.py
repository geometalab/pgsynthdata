import os
import subprocess

from dotenv import load_dotenv

from pgsynthdata.database import Database

# pylint: disable=W0104

load_dotenv()

DBNAME = os.getenv("TEST_DBNAME")
USER = os.getenv("TEST_USER")
PW = os.getenv("TEST_PW")
HOST = os.getenv("TEST_HOST")
PORT = os.getenv("TEST_PORT")
PORT = os.getenv("TEST_PORT")


def _check_database_connection() -> bool:
    try:
        Database(DBNAME, USER, PW, HOST, PORT)
    except:
        return False
    return True


# pylint: disable=W0104
def _check_postgres_client_tools() -> bool:
    try:
        subprocess.run(["pg_restore", "--version"], timeout=1, capture_output=True)
        subprocess.run(["pg_restore", "--version"], timeout=1, capture_output=True)
        return True
    except FileNotFoundError:
        return False


HAS_POSTGRES_CLIENT_TOOLS = _check_postgres_client_tools()
CAN_CONNECT_TO_DATABASE = _check_database_connection()
