import itertools
import os
from datetime import datetime

import matplotlib.pyplot as plt
import numpy as np
import torch
import yaml
from IPython.display import clear_output
from pandas.api.types import is_numeric_dtype
from torch import nn
from torch.utils.data import DataLoader
from yaml import SafeLoader

from .model import PointNet_Discriminator, PointNet_Generator
from .utils import dp_proba, get_sample_points

# pylint: disable=E1101, E1102, C0103

# get data from config file

pg_path = os.path.dirname(os.path.dirname(__file__))
file_path = os.path.join(pg_path, "GeoPointGAN/config.yaml")

with open(file_path, encoding="utf-8") as config:
    config = yaml.load(config, Loader=SafeLoader)
global_config = config["global"]
train_config = config["train"]


# ts = datetime.now().strftime("%Y%m%d_%H%M")
ts = datetime.now().strftime("%Y%m%d")


def train(data_input, c_name):
    # READ ARGS
    enable_plotting = global_config["enable_plotting"]
    device = global_config["device"]

    random_state = train_config["random_state"]
    path = train_config["generator_save_path"]
    model_path = os.path.join(pg_path, path)

    n_samples = train_config["n_samples"]
    n_points = train_config["n_points"]
    draw_with_replacement = train_config["draw_with_replacement"]
    n_epochs = train_config["n_epochs"]
    lr = train_config["learning_rate"]
    embed_dim_d = train_config["embed_dim_discriminator"]
    embed_dim_g = train_config["embed_dim_generator"]
    d_train = train_config["d_train"]
    label_flip = train_config["label_flip"]
    eps = train_config["eps"]
    plot_at_step = train_config["plot_at_step"]
    save_model = train_config["save_model"]
    save_at_step = train_config["save_at_step"]

    # SET DEVICE
    DEVICE = torch.device(device)

    # SET SEED
    torch.manual_seed(random_state)  # Initiate random seed

    # CREATE SAVE PATH
    models_path = os.path.join(pg_path, path)
    if not os.path.exists(models_path):
        os.mkdir(models_path)

    # INITIATE
    print(
        f"Training on column {c_name} - Label flip: {label_flip} - Eps: {round(eps,3)}"
    )

    # SET PROBABILITIES FOR PRIVACY MECHANISM
    p, q = dp_proba(eps, 2)
    if not label_flip:
        eps = -1

    # check for header names, data type and normalize
    data_scaled = data_input.copy()

    for i in data_scaled:
        assert is_numeric_dtype(
            data_scaled[i]
        ), "Column value is not numeric, dataset can not be read."
        data_scaled[i] = (
            2
            * (data_scaled[i] - min(data_scaled[i]))
            / (max(data_scaled[i]) - min(data_scaled[i]))
            - 1
        )

    # Set dimensions
    n_dim = data_input.shape[1]

    # Generate training samples by drawing random samples from the real data
    total_n_points = data_input.shape[0]

    # Generate training labels
    if label_flip:
        D_labels = torch.bernoulli(torch.tensor([p] * total_n_points)).reshape(
            total_n_points, 1
        )
        data_scaled["d_lab"] = D_labels.numpy()

    if label_flip:
        data = torch.zeros(n_samples, n_points, n_dim + 1)
    else:
        data = torch.zeros(n_samples, n_points, n_dim)

    for i in range(n_samples - 1):
        if draw_with_replacement:
            idx = np.random.randint(
                low=0, high=total_n_points, size=n_points
            )  # Draw with replacement
        else:
            idx = np.random.choice(total_n_points, n_points)  # Draw without replacement
        sample = data_scaled.iloc[idx]
        if label_flip:
            sample = sample.to_numpy().reshape(n_points, n_dim + 1)
        else:
            sample = sample.to_numpy().reshape(n_points, n_dim)
        data[i, :, :] = torch.tensor(sample)
    train_data = data.clone().detach()  # torch.tensor(data)

    # Define data loader
    train_loader = DataLoader(
        train_data,
        batch_size=1,
        shuffle=True,
        drop_last=True,
        num_workers=2,
        pin_memory=True,
    )

    # Define G and D
    D = PointNet_Discriminator(code_nfts=embed_dim_d, n_dim=n_dim).to(DEVICE)
    G = PointNet_Generator(code_nfts=embed_dim_g, n_dim=n_dim).to(DEVICE)
    # Loss criterion
    criterion = nn.BCELoss()
    # Initialize D and G optimizer
    D_opt = torch.optim.AdamW(D.parameters(), lr=lr, betas=(0.5, 0.999))
    G_opt = torch.optim.AdamW(G.parameters(), lr=lr, betas=(0.5, 0.999))
    scheduler_D = torch.optim.lr_scheduler.MultiStepLR(
        D_opt, milestones=[5000, 50000, 90000], gamma=0.1
    )
    scheduler_G = torch.optim.lr_scheduler.MultiStepLR(
        G_opt, milestones=[5000, 50000, 90000], gamma=0.1
    )
    # Define fake and real point labels
    D_labels = torch.ones([n_points, 1]).to(DEVICE)  # Discriminator Label to real
    # D_labels = D_labels - 0.1 # Can skip that line - Just a trick known to work well with GANs
    D_fakes = torch.zeros([n_points, 1]).to(DEVICE)  # Discriminator Label to fake

    gen_loss = []
    disc_loss = []
    step = 0  # Initiate training step
    g_step = 0

    z_img = torch.randn(total_n_points, n_dim, 1).to(
        DEVICE
    )  # Initialize random noise for generating images

    for e in range(n_epochs):
        # Within each iteration, we will go over each minibatch of data
        for minibatch_i, (x_batch) in enumerate(train_loader):
            # Get data
            x_batch = x_batch.float().to(DEVICE)
            if label_flip:
                x = x_batch[:, :, :n_dim]
                D_labels = x_batch[:, :, -1].reshape(n_points, 1)
                D_fakes = (
                    torch.bernoulli(torch.tensor([q] * n_points))
                    .reshape(n_points, 1)
                    .to(DEVICE)
                )
            else:
                x = x_batch[:, :, :n_dim]
            x = x.permute(1, 2, 0)

            # Train Discriminator
            for _ in itertools.repeat(
                None, d_train
            ):  # Train Disc d_train steps over Generator
                x_outputs = D(x)
                z = torch.randn(n_points, n_dim, 1).to(DEVICE)
                z_gen, _ = G(z)
                z_gen = z_gen.reshape(n_points, n_dim, 1)
                z_outputs = D(z_gen)
                D_x_loss = criterion(x_outputs, D_labels)
                D_z_loss = criterion(z_outputs, D_fakes)
                D_loss = D_x_loss + D_z_loss
                D.zero_grad()
                D_loss.backward()
                D_opt.step()
                scheduler_D.step()
            # Training Generator
            g_step += 1
            z = torch.randn(n_points, n_dim, 1).to(DEVICE)
            z_gen, _ = G(z)
            z_gen = z_gen.reshape(n_points, n_dim, 1)
            z_outputs = D(z_gen)
            G_z_loss = criterion(z_outputs, D_labels)
            G_loss = G_z_loss
            G.zero_grad()
            G_loss.backward()
            G_opt.step()
            scheduler_G.step()
            gen_loss.append(G_loss.item())
            disc_loss.append(D_loss.item())
            # Training utilities: Saving models, plotting and loss progression
            if save_model:
                if step % save_at_step == 0:
                    gen_path = (
                        rf"{model_path}\G_{c_name}_eps{str(eps)}_ep{n_epochs}_{ts}.pth"
                    )
                    torch.save(G, gen_path)
                    torch.save(
                        D,
                        rf"{model_path}\D_{c_name}_eps{str(eps)}_ep{n_epochs}_{ts}.pth",
                    )
            if enable_plotting and n_dim <= 3:
                if step % plot_at_step == 0:
                    with torch.no_grad():
                        G.eval()
                        exp_points = get_sample_points(G, z_img, n_dim)
                        clear_output()
                        plt.figure(0)
                        fig = plt.figure()

                        if n_dim == 1:
                            ax = fig.add_subplot(1, 1, 1)
                            ax.scatter(
                                exp_points[:, 0],
                                np.zeros_like(exp_points[:, 0]),
                                s=0.1,
                                alpha=0.1,
                                c="black",
                            )
                        elif n_dim == 2:
                            ax = fig.add_subplot(1, 1, 1)
                            ax.scatter(
                                exp_points[:, 0],
                                exp_points[:, 1],
                                s=0.1,
                                alpha=0.1,
                                c="black",
                            )
                        elif n_dim == 3:
                            ax = fig.add_subplot(projection="3d")
                            ax.scatter(
                                exp_points[:, 0],
                                exp_points[:, 1],
                                exp_points[:, 2],
                                s=0.1,
                                alpha=0.1,
                                c="black",
                            )
                        ax.set_facecolor("white")
                        ax.set_title(f"Step: {step}")
                        plt.show(block=False)
                        G.train()
            if step % save_at_step == 0:
                print(
                    rf"Epoch {e+1} [{minibatch_i}/{len(train_loader)}] - G Loss: {round(G_loss.item(), 4)} - D Loss: {round(D_loss.item(), 4)} - Learning rate: {round(D_opt.param_groups[0]['lr'],6)}"
                )
            # Increment step
            step = step + 1

    if enable_plotting:
        print("Close all plot windows to continue.")
        plt.show(block=True)  # leaves plot winddow open
    return os.path.join(os.path.dirname(os.path.dirname(__file__)), gen_path)
