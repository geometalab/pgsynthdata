from math import e

# pylint: disable=C0103

# Plotting
def get_sample_points(G, z, n_dim=2):
    z_gen, _ = G(z)
    z_gen = z_gen.permute(2, 0, 1)
    n = z_gen.shape[1]
    batch_size = z_gen.shape[0]
    y_hat = z_gen.reshape(n * batch_size, n_dim)
    points = y_hat.cpu().data.numpy()
    return points


# Probability calculator with privacy budget
def dp_proba(eps, d):
    p = (e ** eps) / (e ** eps + d - 1)
    q = 1 / (e ** eps + d - 1)
    return p, q
