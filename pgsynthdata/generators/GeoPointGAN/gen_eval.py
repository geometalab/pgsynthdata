import os

import geopandas as gpd
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import torch
import yaml
from pandas.api.types import is_integer_dtype, is_numeric_dtype
from scipy.optimize import linear_sum_assignment
from scipy.spatial.distance import cdist
from shapely.geometry import Point
from sklearn.metrics import mean_absolute_error
from sklearn.neighbors import KernelDensity, NearestNeighbors
from yaml import SafeLoader

from .utils import get_sample_points

# pylint: disable=E1101, C0103

# get data from config file
file_path = os.path.join(
    os.path.dirname(os.path.dirname(__file__)), "GeoPointGAN/config.yaml"
)
with open(file_path, encoding="utf-8") as config:
    config = yaml.load(config, Loader=SafeLoader)
global_config = config["global"]
generation_config = config["generation"]
evaluation_config = config["evaluation"]
feature_config = config["feature_data"]


def generate_points(original_data, path_g):  # creates n_dim synthetic points
    device = global_config["device"]
    denorm = generation_config["denormalize"]
    n_points = generation_config["gen_n_points"]

    n_dim = original_data.shape[1]
    df = original_data.copy()

    for i in original_data:
        assert is_numeric_dtype(
            original_data[i]
        ), "Column value is not numeric, dataset can not be read."
        original_data[i] = (
            2
            * (original_data[i] - min(original_data[i]))
            / (max(original_data[i]) - min(original_data[i]))
            - 1
        )
    if n_points == "all":
        total_n_points = original_data.shape[0]
    else:
        assert int(n_points), "Invalid command."
        total_n_points = int(n_points)
    assert (
        total_n_points <= original_data.shape[0]
    ), "gen_n_points must be smaller or equal than passed data."
    original_data = original_data.sample(total_n_points)
    G = torch.load(path_g, map_location=device)
    G.eval()
    assert (
        G.n_dim == original_data.shape[1]
    ), "Dimensions of original data and trained generator do not match!"
    with torch.no_grad():
        z = torch.randn(total_n_points, n_dim, 1).to(device)
        synthetic_data = get_sample_points(G, z, n_dim)
    synthetic_data = pd.DataFrame(data=synthetic_data)
    if denorm:
        for i in original_data:
            original_data[i] = (original_data[i] + 1) / 2 * (
                max(df[i]) - min(df[i])
            ) + min(df[i])
            synthetic_data[i] = (synthetic_data[i] + 1) / 2 * (
                max(df[i]) - min(df[i])
            ) + min(df[i])
            if is_integer_dtype(df[i]):
                synthetic_data[i] = synthetic_data[i].astype(int)
    return original_data.to_numpy(), synthetic_data.to_numpy()


# compute earthmover distance
def emd(x, y):
    assert x.shape == y.shape, "Invalid shape of input data."
    assert x.shape[0] <= 1000, "To much points to calculate. Reduce amount of rows!"
    if x.shape[1] > 3:
        print(
            "Large dimensions may take up huge computation time or throw an OOM-Error."
        )
    d = cdist(x, y)
    assignment = linear_sum_assignment(d)
    return print(f"Earth Mover's Distance: {round(d[assignment].sum()/x.shape[0],2)}\n")


# compute chamfer distance
def chamfer_distance(x, y, metric="l2", direction="bi"):
    assert x.shape == y.shape, "Invalid shape."
    if x.shape[1] > 3:
        print("Large dimensions may take up indefensible computation time.")
    x = np.array(x, dtype="float32")
    y = np.array(y, dtype="float32")
    if direction == "y_to_x":
        x_nn = NearestNeighbors(
            n_neighbors=1, leaf_size=1, algorithm="kd_tree", metric=metric
        ).fit(x)
        min_y_to_x = x_nn.kneighbors(y)[0]
        chamfer_dist = np.mean(min_y_to_x)
    elif direction == "x_to_y":
        y_nn = NearestNeighbors(
            n_neighbors=1, leaf_size=1, algorithm="kd_tree", metric=metric
        ).fit(y)
        min_x_to_y = y_nn.kneighbors(x)[0]
        chamfer_dist = np.mean(min_x_to_y)
    elif direction == "bi":
        x_nn = NearestNeighbors(
            n_neighbors=1, leaf_size=1, algorithm="kd_tree", metric=metric
        ).fit(x)
        min_y_to_x = x_nn.kneighbors(y)[0]
        y_nn = NearestNeighbors(
            n_neighbors=1, leaf_size=1, algorithm="kd_tree", metric=metric
        ).fit(y)
        min_x_to_y = y_nn.kneighbors(x)[0]
        chamfer_dist = np.mean(min_y_to_x) + np.mean(min_x_to_y)
    else:
        raise ValueError("Invalid direction type. Supported types: 'y_x', 'x_y', 'bi'")
    return print(f"Chamfer Distance: {round(chamfer_dist,3)}\n")


def hotspot(point_data):
    enable_plotting = global_config["enable_plotting"]
    n_grid = evaluation_config["hotspot_grid_size"]
    kernel_width = evaluation_config["kernel_bandwidth"]

    n_dim = point_data.shape[1]
    if n_dim == 1:
        # grid definition
        # x coordinates for corner cells
        x = point_data[:, 0]
        xmin = x.min()
        xmax = x.max()
        # x coordinates of the grid cells, n_gird defines the number of grid cells -1
        xgrid = np.linspace(xmin, xmax, n_grid)
        # y coordinates of the grid cells

        X = np.array(np.meshgrid(xgrid))
        # coordinates of the grid points --> shape = (roundup(N/grid_step)**2, 2)
        xx = np.transpose(X)
        kde = KernelDensity(bandwidth=kernel_width, kernel="gaussian")
        kde.fit(point_data)
        Z = np.exp(
            kde.score_samples(xx)
        )  # score of log-likelihood of each sample in x under the xy gridpoints
        Z = Z.reshape(X.shape)
        return Z

    else:
        # grid definition
        # x,y coordinates for corner cells
        x = point_data[:, 0]
        y = point_data[:, 1]
        xmin = x.min()
        xmax = x.max()
        ymin = y.min()
        ymax = y.max()
        # x coordinates of the grid cells, n_gird defines the number of grid cells -1
        xgrid = np.linspace(xmin, xmax, n_grid)
        # y coordinates of the grid cells
        ygrid = np.linspace(ymin, ymax, n_grid)
        X, Y = np.meshgrid(xgrid, ygrid[::-1])
        # coordinates of the grid points --> shape = (roundup(N/grid_step)**2, 2)
        xy = np.vstack([X.ravel(), Y.ravel()]).T
        kde = KernelDensity(bandwidth=kernel_width, kernel="gaussian")
        kde.fit(point_data[:, :2])
        Z = np.exp(
            kde.score_samples(xy)
        )  # score of log-likelihood of each sample in x under the xy gridpoints
        Z = Z.reshape(X.shape)

        levels = np.linspace(0, Z.max(), 25)
        if enable_plotting:
            plt.figure()
            plt.subplot(1, 1, 1)
            plt.contourf(X, Y, Z, levels=levels, cmap=plt.cm.Reds)
            plt.colorbar()
            plt.scatter(point_data[:, 0], point_data[:, 1], alpha=0.05, s=0.05, c="k")
            plt.scatter(xy[:, 0], xy[:, 1], alpha=0.35, s=1, c="b", marker="+")
        return Z


def sorensen_dice(original_data, synthetic_data):
    n_dim = original_data.shape[1]
    threshold = evaluation_config["hotspot_threshold"]
    assert original_data.shape == synthetic_data.shape, "Invalid data shape."
    if n_dim > 2:
        print(
            "Only the first two features of multi-dimensional input will be analyzed."
        )
    if n_dim == 1:
        print("Hotspot plot not available for 1d array.")
    orig_ll = hotspot(original_data)
    if n_dim >= 2:
        plt.show(block=False)
        plt.title("Original Data")
    synth_ll = hotspot(synthetic_data)
    if n_dim >= 2:
        plt.show(block=False)
        plt.title("Synthetic Data")
    Hr = np.where(orig_ll.flatten() >= threshold)
    Hs = np.where(synth_ll.flatten() >= threshold)
    if (len(Hr[0]) + len(Hs[0])) != 0:
        sdc = 2 * (len(np.intersect1d(Hr, Hs))) / (len(Hr[0]) + len(Hs[0]))
        if n_dim >= 2:
            plt.show(block=True)
        return print(f"Sorensen Dice Value: {round(sdc,3)}\n")
    else:
        if n_dim >= 2:
            plt.show(block=True)
        return print(
            "No loglikeleihood value above threshold to compute Sorensen Dice Value.\n"
        )


def mae(orig, synth):
    # append smaller array with the average values to the same size
    if orig.shape[0] > synth.shape[0] and orig.shape[0] != 0 and synth.shape[0] != 0:
        dif = orig.shape[0] - synth.shape[0]
        mx, my = np.mean(synth, axis=0)
        arr_x, arr_y = np.full((dif), mx), np.full((dif), my)
        arr = np.stack((arr_x, arr_y), axis=1)
        synth = np.append(synth, arr, axis=0)
        return print(
            f"Mean absolute error: {round(mean_absolute_error(orig, synth),3)}\n"
        )
    elif orig.shape[0] < synth.shape[0] and orig.shape[0] != 0 and synth.shape[0] != 0:
        dif = synth.shape[0] - orig.shape[0]
        mx, my = np.mean(orig, axis=0)
        arr_x, arr_y = np.full((dif), mx), np.full((dif), my)
        arr = np.stack((arr_x, arr_y), axis=1)
        orig = np.append(orig, arr, axis=0)
        return print(
            f"Mean absolute error: {round(mean_absolute_error(orig, synth),3)}\n"
        )
    elif orig.shape[0] == synth.shape[0] and orig.shape[0] != 0 and synth.shape[0] != 0:
        return print(
            f"Mean absolute error: {round(mean_absolute_error(orig, synth),3)}\n"
        )


def range_query(orig, synth):
    enable_plotting = global_config["enable_plotting"]
    radius = evaluation_config["range_query_radius"]
    assert orig.shape == synth.shape, "Invalid data shape."

    n_dim = orig.shape[1]

    if n_dim == 1:
        # random choice of original data
        idx = np.random.randint(len(orig))
        x = orig[idx, 0]
        y = np.zeros_like(x)
        xh = np.max(orig[:, 0])
        xl = np.min(orig[:, 0])
        dx = xh - xl
        d = dx * 0.5

        points1 = gpd.GeoSeries([Point(i, [0]) for i in orig])
        points2 = gpd.GeoSeries([Point(i, [0]) for i in synth])
        circle = Point(x, y).buffer(
            radius
        )  # negative buffer value aka radius is not included (half open intervall)
        mask1 = points1.intersects(circle)
        mask2 = points2.intersects(circle)
        orig_rq, synth_rq = points1[mask1], points2[mask2]

        if enable_plotting:
            u, v = circle.exterior.xy
            # plt.figure(figsize=(6,6))
            plt.plot(u, v, c="r")
            plt.plot(x, y, c="r", marker="+")
            # plt.scatter(orig[:,0], np.zeros_like(orig[:,0]), alpha=0.05, marker='.', s=0.05, c='k')
            plt.scatter(
                synth[:, 0],
                np.zeros_like(synth[:, 0]),
                alpha=0.05,
                marker=".",
                s=0.05,
                c="b",
            )
            plt.title("Range Query")
            plt.show()

        # transform back to numpy array
        if orig_rq.shape[0] != 0 and synth_rq.shape[0] != 0:
            orig_x, orig_y = np.array(orig_rq.x), np.array(orig_rq.y)
            orig_rqnp = np.stack((orig_x, orig_y), axis=1)
            synth_x, synth_y = np.array(synth_rq.x), np.array(synth_rq.y)
            synth_rqnp = np.stack((synth_x, synth_y), axis=1)
            # compute mean average error
            return mae(orig_rqnp, synth_rqnp)

        elif orig_rq.shape[0] == 0 or synth_rq.shape[0] == 0:
            return print(
                "Can not compute mean average error since one of the range query outputs is empty."
            )

    else:
        if n_dim > 2:
            print(
                "Only the first two features of multi-dimensional input will be analyzed."
            )
        # random choice of original data
        idx = np.random.randint(len(orig))
        x, y = orig[idx, 0], orig[idx, 1]

        xh, yh = np.max(orig[:, 0]), np.max(orig[:, 1])
        xl, yl = np.min(orig[:, 0]), np.min(orig[:, 1])
        dx, dy = xh - xl, yh - yl
        if dx >= dy:
            d = dx * 0.5
        else:
            d = dy * 0.5

        points1 = gpd.GeoSeries([Point(i) for i in orig[:, :2]])
        points2 = gpd.GeoSeries([Point(i) for i in synth[:, :2]])
        circle = Point(x, y).buffer(
            radius
        )  # negative buffer value aka radius is not included (half open intervall)
        mask1 = points1.intersects(circle)
        mask2 = points2.intersects(circle)
        orig_rq, synth_rq = points1[mask1], points2[mask2]

        if enable_plotting:
            u, v = circle.exterior.xy
            # plt.figure(figsize=(6,6))
            plt.plot(u, v, c="r")
            plt.plot(x, y, c="r", marker="+")
            # plt.scatter(orig[:,0], orig[:,1], alpha=0.05, marker='.', s=0.05, c='k')
            plt.scatter(synth[:, 0], synth[:, 1], alpha=0.05, marker=".", s=0.05, c="b")
            plt.xlim((x - radius - d, x + radius + d))
            plt.ylim((y - radius - d, y + radius + d))
            plt.title("Range Query")
            plt.show()

        # transform back to numpy array
        if orig_rq.shape[0] != 0 and synth_rq.shape[0] != 0:
            orig_x, orig_y = np.array(orig_rq.x), np.array(orig_rq.y)
            orig_rqnp = np.stack((orig_x, orig_y), axis=1)
            synth_x, synth_y = np.array(synth_rq.x), np.array(synth_rq.y)
            synth_rqnp = np.stack((synth_x, synth_y), axis=1)
            # compute mean average error
            return mae(orig_rqnp, synth_rqnp)

        elif orig_rq.shape[0] == 0 or synth_rq.shape[0] == 0:
            return print(
                "Can not compute mean average error since one of the range query outputs is empty."
            )


def eval_data(orig, synth):
    method = evaluation_config["method"]

    if method == "emd":
        return emd(orig, synth)
    elif method == "cd":
        return chamfer_distance(orig, synth)
    elif method == "sorensen_dice":
        return sorensen_dice(orig, synth)
    elif method == "range_query":
        return range_query(orig, synth)
