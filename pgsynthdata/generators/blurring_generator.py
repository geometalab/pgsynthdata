import numpy as np
import pandas as pd
from pandas.api.types import is_numeric_dtype
from shapely import from_wkb

from ..datagenerator import (
    AsIs,
    ColumnPgInfo,
    Constraints,
    StatisticQuery,
    Statistics,
    generator,
)
from .blurring_helper.gen_eval import eval_data, evaluation_config, global_config

# pylint: disable=W0613, C0103


@generator(
    "USER-DEFINED",
    comment_identifier="BLUR",
    udt_names_only={"geometry"},
    custom_queries=[
        StatisticQuery(
            name="src_data",
            query="SELECT {column_name} FROM {table_name}",
        ),
    ],
)
def blurring_generator(
    stats: Statistics, column_info: ColumnPgInfo, constraints: Constraints, rows_to_gen
):
    point_data = stats.custom_stats["src_data"]
    point_data = pd.DataFrame(
        [
            [from_wkb(i)[0].x, from_wkb(i)[0].y]
            for i in point_data
            if from_wkb(i)[0].geom_type == "Point"
        ]
    )

    # normalize data
    if evaluation_config["normalize"]:
        for i in point_data:
            assert is_numeric_dtype(
                point_data[i]
            ), "Column value is not numeric, dataset can not be read."
            point_data[i] = (
                2
                * (point_data[i] - min(point_data[i]))
                / (max(point_data[i]) - min(point_data[i]))
                - 1
            )
    point_data = point_data.to_numpy()
    orig = point_data.copy()

    decimal_points = 3
    precision = 0.0001
    radius = global_config["blurring_radius"]
    try:
        radius = float(radius)
    except (ValueError, TypeError):
        print("Please supply a valid float value as blurring radius.")
    assert precision < radius, "Precision is higher than selected radius."

    # arange possible values of the circle
    r = np.arange(0, radius + precision, precision)
    phi = np.arange(0, 360 + precision, precision)

    # set empty array of synthetic data
    synth = np.array([])
    r_choosen = np.array([])
    shape = point_data.shape

    # loop over points
    for point in point_data:
        rad = np.random.choice(r)
        Phi = np.random.choice(phi)
        synth_i = [point[0] + rad * np.cos(Phi), point[1] + rad * np.sin(Phi)]
        r_choosen = np.append(r_choosen, rad)
        synth = np.append(synth, synth_i)
    synth = synth.reshape((shape))

    if evaluation_config["eval"]:
        assert (
            evaluation_config["eval"] == evaluation_config["normalize"]
        ), "For Evaluation, set normalize to true."
        if global_config["enable_plotting"] and (
            evaluation_config["method"] in ("sorensen_dice", "range_query")
        ):
            print("Close all plot windows to continue.")
        eval_data(orig, synth)  # input data as numpy

    point_data = pd.DataFrame(synth).round(decimals=decimal_points)
    geo_x = point_data.iloc[:, 0].to_list()
    geo_y = point_data.iloc[:, 1].to_list()
    point_data = [AsIs(f"ST_POINT({x},{y})") for x, y in zip(geo_x, geo_y)]

    return point_data
