import os

import pandas as pd
from shapely import from_wkb

from ..datagenerator import (
    AsIs,
    ColumnPgInfo,
    Constraints,
    StatisticQuery,
    Statistics,
    generator,
)
from .GeoPointGAN.gen_eval import (
    eval_data,
    evaluation_config,
    feature_config,
    generate_points,
    generation_config,
)
from .GeoPointGAN.train import train, train_config

# pylint: disable=C0103


@generator(
    None,
    comment_identifier="GAN",
    custom_queries=[
        StatisticQuery(
            name="src_data",
            query="""SELECT {column_name} FROM {table_name}""",
        ),
    ],
)
def gan_generator(
    # pylint: disable=W0613
    stats: Statistics,
    column_info: ColumnPgInfo,
    constraints: Constraints,
    rows_to_gen,
):
    # get column data types and names
    c_dtype = column_info.data_type
    r_dtype = column_info.udt_name
    c_name = column_info.column_name

    decimal_points = 3
    path_g = generation_config["path_to_generator"]

    # check for point data and select said to train gan
    if c_dtype == "USER-DEFINED" and r_dtype == "geometry":
        point_data = stats.custom_stats["src_data"]
        # check point type and create list
        point_data = pd.DataFrame(
            [
                [from_wkb(i)[0].x, from_wkb(i)[0].y]
                for i in point_data
                if from_wkb(i)[0].geom_type == "Point"
            ]
        )

        # train gan and select correct path for synthetic data generatrion
        if train_config["train_new_gan"]:
            gen_path = train(point_data, c_name)  # input data as dataframe
        elif not train_config["train_new_gan"] and path_g == "":
            assert (
                train_config["train_new_gan"] and path_g != ""
            ), "Supply a path to a valid generator or switch boolean in train_new_gan in the config file."
        if path_g != "":
            # select first index in path list where column name occours
            idx = [idx for idx, s in enumerate(path_g) if c_name in s][0]
            gen_path = os.path.join(
                os.path.abspath(os.getcwd()),
                train_config["generator_save_path"],
                path_g[idx],
            )

        # generate synthetic data, returned as numpy array
        orig, synth = generate_points(point_data, gen_path)

        # evaluation of synthetic and original data
        if evaluation_config["eval"]:
            assert (
                evaluation_config["eval"] != generation_config["denormalize"]
            ), "For Evaluation, set denormalize to false."
            eval_data(orig, synth)  # input data as numpy

        # preprocess and upload pointdata to postgis database
        point_data = pd.DataFrame(synth).round(decimals=decimal_points)
        geo_x = point_data.iloc[:, 0].to_list()
        geo_y = point_data.iloc[:, 1].to_list()
        point_data = [AsIs(f"ST_POINT({x},{y})") for x, y in zip(geo_x, geo_y)]
        return point_data

    # check for feature data and select said for train gan
    elif c_dtype in (
        "integer",
        "numeric",
        "float",
        "double precision",
        "money",
        "real",
        "character varying",
        "character",
    ):
        feature_data = pd.DataFrame(stats.custom_stats["src_data"])

        # try to convert feature to float if varchar and fill with mean or only select not null values
        n_rows = feature_data.shape[0]
        if feature_config["fill_na_mean"]:
            for r in range(n_rows):
                try:
                    feature_data.iloc[r, :] = float(feature_data.iloc[r, :])
                except (ValueError, TypeError):
                    feature_data.iloc[r, :] = None
            feature_data_values = feature_data.fillna(feature_data.mean())
        else:
            for r in range(n_rows):
                try:
                    feature_data.iloc[r, :] = float(feature_data.iloc[r, :])
                except (ValueError, TypeError):
                    feature_data.iloc[r, :] = None
            mask_nan = feature_data.isnull()
            feature_data_values = feature_data[~feature_data.iloc[:, 0].isna()]
            feature_data_values = feature_data_values.astype(float)

        # train gan and select correct path for synthetic data generatrion
        if train_config["train_new_gan"]:
            gen_path = train(feature_data_values, c_name)  # input data as dataframe
        elif not train_config["train_new_gan"] and path_g == "":
            assert (
                train_config["train_new_gan"] and path_g != ""
            ), "Supply a path to a valid generator or switch boolean in train_new_gan in the config file."
        if path_g != "":
            # select first index in path list where column name occours
            idx = [idx for idx, s in enumerate(path_g) if c_name in s][0]
            gen_path = os.path.join(
                os.path.abspath(os.getcwd()),
                train_config["generator_save_path"],
                path_g[idx],
            )

        # generate synthetic data

        orig, synth = generate_points(
            feature_data_values, gen_path
        )  # returned as numpy

        # evaluation of synthetic and original data
        if evaluation_config["eval"]:
            assert (
                evaluation_config["eval"] != generation_config["denormalize"]
            ), "For Evaluation, set denormalize to false."
            eval_data(orig, synth)  # input data as numpy

        if not feature_config["fill_na_mean"]:
            synth = pd.DataFrame(synth).round(decimals=decimal_points)
            feature_data.where(mask_nan, other=synth, inplace=True)
            synth = feature_data.values.tolist()

        feature_data = [
            None if i[0] is None else round(float(i[0]), decimal_points) for i in synth
        ]
        return feature_data
